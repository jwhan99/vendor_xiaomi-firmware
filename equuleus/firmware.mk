# Firmware
PRODUCT_COPY_FILES += \
    vendor/xiaomi-firmware/equuleus/abl.elf:install/firmware-update/abl.elf \
    vendor/xiaomi-firmware/equuleus/aop.img:install/firmware-update/aop.img \
    vendor/xiaomi-firmware/equuleus/bluetooth.img:install/firmware-update/bluetooth.img \
    vendor/xiaomi-firmware/equuleus/cmnlib.img:install/firmware-update/cmnlib.img \
    vendor/xiaomi-firmware/equuleus/cmnlib64.img:install/firmware-update/cmnlib64.img \
    vendor/xiaomi-firmware/equuleus/devcfg.img:install/firmware-update/devcfg.img \
    vendor/xiaomi-firmware/equuleus/dsp.img:install/firmware-update/dsp.img \
    vendor/xiaomi-firmware/equuleus/hyp.img:install/firmware-update/hyp.img \
    vendor/xiaomi-firmware/equuleus/keymaster.img:install/firmware-update/keymaster.img \
    vendor/xiaomi-firmware/equuleus/logo.img:install/firmware-update/logo.img \
    vendor/xiaomi-firmware/equuleus/modem.img:install/firmware-update/modem.img \
    vendor/xiaomi-firmware/equuleus/qupfw.img:install/firmware-update/qupfw.img \
    vendor/xiaomi-firmware/equuleus/storsec.img:install/firmware-update/storsec.img \
    vendor/xiaomi-firmware/equuleus/tz.img:install/firmware-update/tz.img \
    vendor/xiaomi-firmware/equuleus/xbl_config.img:install/firmware-update/xbl_config.img \
    vendor/xiaomi-firmware/equuleus/xbl.img:install/firmware-update/xbl.img
